class sshd {

package {'openssh-server':
	ensure => installed,
}

file {'/etc/ssh/ssh_config':
	source => 'puppet:///modules/sshd/sshd_config',
	owner => 'root',
	group => 'root',
	mode => '640',
	notify => Service['ssh'], #sshd will restart whenever you edit this file.
	require => Package['openssh-server'],
}

service {'ssh':
	ensure => running,
	enable => true,
	hasstatus => true,
	hasrestart => true,
}
}
